﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections.Generic;
using System.IO;
using System;
using System.Text.RegularExpressions;

public class GameController : MonoBehaviour
{

    public enum AppState
    {
        CheckIn, CheckOut
    }

    #region Declare Variables

    private const string URL = "http://pepsi.focusmarketing.vn/PEPSI_TET_2016/Module/";

    private bool isDemoMode = true;
    public bool IsDemoMode
    {
        get
        {
            return isDemoMode;
        }
        set
        {
            isDemoMode = value;
            if (isDemoMode)
            {
                CanvasGroupThumbnailImage.alpha = 0;
                CameraCaptureImage.GetComponent<CanvasGroup>().alpha = 1;
            }
            else
            {
                CanvasGroupThumbnailImage.alpha = 1;
                CameraCaptureImage.GetComponent<CanvasGroup>().alpha = 0;
            }
        }
    }

    public Button BtnNext, BtnCapture, BtnCheckIn, BtnCheckOut, BtnInfo, BtnTracking;

    public InputField InputFieldName;

    public Toggle ToggleMechan, ToggleMechanBigC, ToggleMechanPG;

    public RectTransform RTPanelInformation, RTPanelCapture;

    public RawImage Camera;
    public RawImage CameraCaptureImage;
    public CanvasGroup CanvasGroupThumbnailImage;

    private RectTransform rtBtnNext, rtBtnCapture, rtBtnCheckIn, rtBtnCheckOut;

    public RawImage[] ThumbnailImages;

    public Button BtnChangeMode;

    private bool isShowInfo = true;

    private int imageCaptureNumber = 0;
    private string[] oldNameFile = new[] { "", "", "" };
    private List<byte[]> imageContents = new List<byte[]>();

    private WebCamTexture webcamTexture;

    private Tween tweenSimulate;

    private double longtitude, latitude;
    private int idType;
    private string tracking_id1;
    private string tracking_id2;

    private AppState appState;

    private string fullName;


    private string inputString = "";
    private Coroutine coroutineChangeMode;

    private int stepCheckInOut = 0;

    #endregion Declare Variables

    private void Awake()
    {
        //PlayerPrefs.DeleteAll();
        WebCamDevice[] devices = WebCamTexture.devices;

        InitializeButton();

        appState = AppState.CheckIn;

        if (devices.Length > 0)
        {
            if (devices.Length > 1)
            {
                Camera.GetComponent<RectTransform>().localScale = new Vector3(-1, 1, 1);
            }
            webcamTexture = new WebCamTexture(devices[devices.Length - 1].name);
        }

        Camera.texture = webcamTexture;
        webcamTexture.Play();

        fullName = PlayerPrefs.GetString("FullName", "");
        tracking_id1 = PlayerPrefs.GetString("TrackingID1", "");
        tracking_id2 = PlayerPrefs.GetString("TrackingID2", "");
        stepCheckInOut = PlayerPrefs.GetInt("Step", 0);
        IsDemoMode = bool.Parse(PlayerPrefs.GetString("IsDemoMode", bool.TrueString));

        if (stepCheckInOut != 0)
        {
            InputFieldName.interactable = false;
            if (stepCheckInOut == 1)
            {
                appState = AppState.CheckIn;
                BtnCheckIn.interactable = false;
                BtnCheckOut.interactable = true;
            }
            else if(stepCheckInOut == 2)
            {
                appState = AppState.CheckOut;
                BtnCheckIn.interactable = true;
                BtnCheckOut.interactable = false;
            }
            else
            {
                appState = AppState.CheckOut;
                BtnCheckIn.interactable = false;
                BtnCheckOut.interactable = true;
            }
        }
        else
        {
            InputFieldName.interactable = true;
            appState = AppState.CheckIn;
            BtnCheckIn.interactable = true;
            BtnCheckOut.interactable = false;
        }

        Debug.Log(stepCheckInOut + " | " + tracking_id1 + " | " + tracking_id2);
        InputFieldName.text = fullName;
    }

    private void InitializeButton()
    {
        rtBtnNext = BtnNext.GetComponent<RectTransform>();
        rtBtnCapture = BtnCapture.GetComponent<RectTransform>();
        rtBtnCheckIn = BtnCheckIn.GetComponent<RectTransform>();
        rtBtnCheckOut = BtnCheckOut.GetComponent<RectTransform>();

        BtnNext.onClick.AddListener(() =>
        {
            if (InputFieldName.text.Trim() != "")
            {
                SwitchPanelTracking();
            }
        });

        BtnCapture.onClick.AddListener(() =>
        {
            CaptureImage();
        });

        BtnCheckIn.onClick.AddListener(() =>
        {
            CheckIn();
        });

        BtnCheckOut.onClick.AddListener(() =>
        {
            CheckOut();
        });

        BtnInfo.onClick.AddListener(() =>
        {
            SwitchPanelInfo();
        });

        BtnTracking.onClick.AddListener(() =>
        {
            if (InputFieldName.text.Trim() != "")
            {
                SwitchPanelTracking();
            }
        });

        ToggleMechan.onValueChanged.AddListener(isOn =>
        {
            idType = 0;
        });

        ToggleMechanBigC.onValueChanged.AddListener(isOn =>
        {
            idType = 1;
        });

        ToggleMechanPG.onValueChanged.AddListener(isOn =>
        {
            idType = 2;
        });

        BtnChangeMode.onClick.AddListener(() =>
            {
                ChangeMode();
            });
    }

    private void ChangeMode()
    {
        inputString += "a";

        if (coroutineChangeMode == null)
        {
            coroutineChangeMode = StartCoroutine(DoChangeMode());
        }
    }

    private IEnumerator DoChangeMode()
    {
        yield return new WaitForSeconds(3);
        if (inputString.Contains("aaaaaaaaaa"))
        {
            IsDemoMode = !IsDemoMode;
            PlayerPrefs.SetString("IsDemoMode", IsDemoMode.ToString());
        }
        inputString = "";

        StopCoroutine(coroutineChangeMode);
        coroutineChangeMode = null;
    }

    IEnumerator Start()
    {
        // First, check if user has location service enabled
        if (!Input.location.isEnabledByUser)
            yield break;

        // Start service before querying location
        Input.location.Start();

        // Wait until service initializes
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            print("Timed out");
            yield break;
        }
    }

    private void SimulateUpdate()
    {
        tweenSimulate = DOVirtual.DelayedCall(1f, () =>
            {
                if (Input.location.status == LocationServiceStatus.Failed)
                {
                    print("Unable to determine device location");
                    tweenSimulate.Kill();
                    tweenSimulate = null;
                }
                else
                {
                    longtitude = Input.location.lastData.longitude;
                    latitude = Input.location.lastData.latitude;
                    print("Location: " + Input.location.lastData.latitude + " " + Input.location.lastData.longitude + " " + Input.location.lastData.altitude + " " + Input.location.lastData.horizontalAccuracy + " " + Input.location.lastData.timestamp);
                }
            }).SetLoops(-1);
    }

    private void OnDisable()
    {
        if (tweenSimulate != null)
        {
            tweenSimulate.Kill();
            tweenSimulate = null;
        }
        Input.location.Stop();
    }

    private void OnEnable()
    {
        StartCoroutine(Start());
        SimulateUpdate();
    }

    public void CheckIn()
    {
        if (stepCheckInOut == 0)
        {
            if (IsDemoMode)
            {
                if (InputFieldName.text.Trim() == "" || imageCaptureNumber < 1)
                {
                    return;
                }
            }
            else
            {
                if (InputFieldName.text.Trim() == "" || imageCaptureNumber < 3)
                {
                    return;
                }
            }

            BtnCheckIn.interactable = false;

            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("user_id", GeneralVariables.USERID);
            data.Add("position", idType.ToString());
            data.Add("full_name", InputFieldName.text.Trim());
            data.Add("longtitude", longtitude.ToString());
            data.Add("latitude", latitude.ToString());
            data.Add("statuslunch", "0");
            Dictionary<string, byte[]> images = new Dictionary<string, byte[]>();
            if (IsDemoMode)
            {
                images.Add(oldNameFile[0] + "0", imageContents[0]);
                images.Add(oldNameFile[0] + "1", imageContents[0]);
                images.Add(oldNameFile[0] + "2", imageContents[0]);
            }
            else
            {
                images.Add(oldNameFile[0], imageContents[0]);
                images.Add(oldNameFile[1], imageContents[1]);
                images.Add(oldNameFile[2], imageContents[2]);
            }

            Request("CheckIn2.php", data, images);
        }
        else if (stepCheckInOut == 2)
        {
            if (IsDemoMode)
            {
                if (tracking_id2 == "" || imageCaptureNumber < 1)
                {
                    return;
                }
            }
            else
            {
                if (tracking_id2 == "" || imageCaptureNumber < 3)
                {
                    return;
                }
            }

            BtnCheckIn.interactable = false;

            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("tracking_id", tracking_id2);
            data.Add("longtitude", longtitude.ToString());
            data.Add("latitude", latitude.ToString());

            Dictionary<string, byte[]> images = new Dictionary<string, byte[]>();
            if (IsDemoMode)
            {
                images.Add(oldNameFile[0] + "0", imageContents[0]);
                images.Add(oldNameFile[0] + "1", imageContents[0]);
                images.Add(oldNameFile[0] + "2", imageContents[0]);
            }
            else
            {
                images.Add(oldNameFile[0], imageContents[0]);
                images.Add(oldNameFile[1], imageContents[1]);
                images.Add(oldNameFile[2], imageContents[2]);
            }
            Request("CheckOut.php", data, images);
        }

    }

    public void CheckOut()
    {
        if (stepCheckInOut == 1)
        {
            if (IsDemoMode)
            {
                if (InputFieldName.text.Trim() == "" || imageCaptureNumber < 1)
                {
                    return;
                }
            }
            else
            {
                if (InputFieldName.text.Trim() == "" || imageCaptureNumber < 3)
                {
                    return;
                }
            }

            BtnCheckOut.interactable = false;

            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("user_id", GeneralVariables.USERID);
            data.Add("position", idType.ToString());
            data.Add("full_name", InputFieldName.text.Trim());
            data.Add("longtitude", longtitude.ToString());
            data.Add("latitude", latitude.ToString());
            data.Add("statuslunch", "1");
            Dictionary<string, byte[]> images = new Dictionary<string, byte[]>();
            if (IsDemoMode)
            {
                images.Add(oldNameFile[0] + "0", imageContents[0]);
                images.Add(oldNameFile[0] + "1", imageContents[0]);
                images.Add(oldNameFile[0] + "2", imageContents[0]);
            }
            else
            {
                images.Add(oldNameFile[0], imageContents[0]);
                images.Add(oldNameFile[1], imageContents[1]);
                images.Add(oldNameFile[2], imageContents[2]);
            }

            Request("CheckIn2.php", data, images);
        }
        else if (stepCheckInOut == 3)
        {
            if (IsDemoMode)
            {
                if (tracking_id1 == "" || imageCaptureNumber < 1)
                {
                    return;
                }
            }
            else
            {
                if (tracking_id1 == "" || imageCaptureNumber < 3)
                {
                    return;
                }
            }

            BtnCheckOut.interactable = false;

            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("tracking_id", tracking_id1);
            data.Add("longtitude", longtitude.ToString());
            data.Add("latitude", latitude.ToString());

            Dictionary<string, byte[]> images = new Dictionary<string, byte[]>();
            if (IsDemoMode)
            {
                images.Add(oldNameFile[0] + "0", imageContents[0]);
                images.Add(oldNameFile[0] + "1", imageContents[0]);
                images.Add(oldNameFile[0] + "2", imageContents[0]);
            }
            else
            {
                images.Add(oldNameFile[0], imageContents[0]);
                images.Add(oldNameFile[1], imageContents[1]);
                images.Add(oldNameFile[2], imageContents[2]);
            }
            Request("CheckOut.php", data, images);
        }

    }

    public void CheckOut2()
    {
        if (tracking_id1 != "")
        {
            BtnCheckOut.interactable = false;

            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("tracking_id", tracking_id1);
            Request("CheckOut2.php", data);
        }
    }

    private void Request(string typeRequest, Dictionary<string, string> data, Dictionary<string, byte[]> images = null)
    {
        WWWForm form = new WWWForm();
        Dictionary<string, string> headers = form.headers;
        headers["Content-Type"] = "application/json";

        foreach (var pair in data)
        {
            form.AddField(pair.Key, pair.Value);
        }

        if (images != null)
        {
            int id = 1;
            foreach (var pair in images)
            {
                form.AddBinaryData("image_" + id, pair.Value, pair.Key, "image/jpg");
                id++;
            }
        }

        WWW www = new WWW(URL + typeRequest, form);
        StartCoroutine(WaitForRequest(www));
    }

    IEnumerator WaitForRequest(WWW www)
    {
        yield return www;
        // check for errors
        if (www.error == null)
        {
            Debug.Log("WWW Ok!: " + www.text + " | " + stepCheckInOut);
            if (www.text.Contains("\"status\":1"))
            {
                if (appState == AppState.CheckIn)
                {
                    Regex regex = new Regex("\"tracking_id\":\\d+");

                    if (stepCheckInOut == 0)
                    {
                        tracking_id1 = regex.Match(www.text).ToString().Replace("\"", "").Replace("tracking_id:", "");
                        PlayerPrefs.SetString("TrackingID1", tracking_id1);
                    }
                    else
                    {
                        tracking_id2 = regex.Match(www.text).ToString().Replace("\"", "").Replace("tracking_id:", "");
                        PlayerPrefs.SetString("TrackingID2", tracking_id2);
                    }

                    Debug.Log("tracking_id: " + tracking_id1 + " | " + tracking_id2);

                    PlayerPrefs.SetString("FullName", InputFieldName.text.Trim());
                    InputFieldName.interactable = false;

                    SwitchPanelInfo();
                    ResetCheckIn();
                    if (stepCheckInOut == 1)
                    {
                        appState = AppState.CheckOut;
                        BtnCheckIn.interactable = true;
                        BtnCheckOut.interactable = false;
                    }
                    stepCheckInOut++;
                    PlayerPrefs.SetInt("Step", stepCheckInOut);
                }
                else
                {
                    if (stepCheckInOut == 3)
                    {
                        Reset();
                        PlayerPrefs.SetString("TrackingID1", "");
                        PlayerPrefs.SetString("TrackingID2", "");
                        InputFieldName.interactable = true;
                        BtnCheckIn.interactable = true;
                        BtnCheckOut.interactable = false;
                        webcamTexture.Stop();
                        Input.location.Stop();

                        DOVirtual.DelayedCall(0.5f, () =>
                        {
                            Application.LoadLevel("Login");
                        });
                        yield break;
                    }
                    else
                    {
                        BtnCheckIn.interactable = false;
                        BtnCheckOut.interactable = true;

                        SwitchPanelInfo();
                        ResetCheckOut();
                    }

                    stepCheckInOut++;
                    PlayerPrefs.SetInt("Step", stepCheckInOut);
                }
            }
        }
        else
        {
            Debug.Log("WWW Error: " + www.error);

            if (appState == AppState.CheckIn)
            {
                BtnCheckIn.interactable = true;
                BtnCheckOut.interactable = false;
            }
            else
            {
                BtnCheckIn.interactable = false;
                BtnCheckOut.interactable = true;
            }
        }
    }

    private void SwitchPanelInfo()
    {
        if (!isShowInfo)
        {
            isShowInfo = true;
            RTPanelInformation.DOAnchorPos(new Vector2(0, 0), 0.5f);
            RTPanelCapture.DOAnchorPos(new Vector2(1024, 0), 0.5f);

            rtBtnNext.DOAnchorPos(new Vector2(0, -230), 0.5f);
            rtBtnCapture.DOAnchorPos(new Vector2(0, -365), 0.5f);
            rtBtnCheckIn.DOAnchorPos(new Vector2(-270, -365), 0.5f);
            rtBtnCheckOut.DOAnchorPos(new Vector2(270, -365), 0.5f);
        }
    }

    private void SwitchPanelTracking()
    {
        if (isShowInfo)
        {
            isShowInfo = false;
            RTPanelInformation.DOAnchorPos(new Vector2(-1024, 0), 0.5f);
            RTPanelCapture.DOAnchorPos(new Vector2(0, 0), 0.5f);

            rtBtnNext.DOAnchorPos(new Vector2(0, -365), 0.5f);
            rtBtnCapture.DOAnchorPos(new Vector2(0, -230), 0.5f);
            rtBtnCheckIn.DOAnchorPos(new Vector2(-270, -230), 0.5f);
            rtBtnCheckOut.DOAnchorPos(new Vector2(270, -230), 0.5f);
        }
    }

    private void CaptureImage()
    {
        if (imageCaptureNumber >= 3)
        {
            return;
        }

        BtnCapture.interactable = false;

        int width = webcamTexture.width;
        int height = webcamTexture.height;

        Texture2D texSave = new Texture2D(width, height);
        texSave.SetPixels(webcamTexture.GetPixels());

        Color[] pix = texSave.GetPixels(width / 6, height / 6, width / 3 * 2, height / 3 * 2);
        Texture2D destTex = new Texture2D(width / 3 * 2, height / 3 * 2);
        destTex.SetPixels(pix);
        destTex.Apply();

        byte[] bytes = FlipHorizontal(destTex).EncodeToJPG(50);
        imageContents.Add(bytes);

        oldNameFile[imageCaptureNumber] = "_" + GeneralVariables.USERNAME + "_" + InputFieldName.text.Trim() + "_" + DateTime.Now.Ticks + ".jpg";

        DOVirtual.DelayedCall(0.5f, () =>
            {
                BtnCapture.interactable = true;
                LoadTexture(ThumbnailImages[imageCaptureNumber], bytes);
                imageCaptureNumber++;
            });
    }

    private void LoadTexture(RawImage image, byte[] bytes)
    {
        image.enabled = true;
        Texture2D tex = new Texture2D(webcamTexture.width, webcamTexture.height);
        tex.LoadImage(bytes);
        image.texture = tex;
        CameraCaptureImage.texture = tex;
        CameraCaptureImage.color = new Color(1, 1, 1, 1);
    }

    Texture2D FlipTexture(Texture2D original)
    {
        Texture2D flipped = new Texture2D(original.width, original.height);

        int xN = original.width;
        int yN = original.height;


        for (int i = 0; i < xN; i++)
        {
            for (int j = 0; j < yN; j++)
            {
                flipped.SetPixel(i, yN - j - 1, original.GetPixel(i, j));
            }
        }
        flipped.Apply();

        return flipped;
    }

    Texture2D FlipHorizontal(Texture2D original)
    {
        Texture2D flipped = new Texture2D(original.width, original.height);

        int xN = original.width;
        int yN = original.height;


        for (int i = 0; i < xN; i++)
        {
            for (int j = 0; j < yN; j++)
            {
                flipped.SetPixel(xN - i - 1, j, original.GetPixel(i, j));
            }
        }
        flipped.Apply();

        return flipped;
    }

    private void Reset()
    {
        stepCheckInOut = 0;
        appState = AppState.CheckIn;
        PlayerPrefs.SetInt("Step", 0);
        ThumbnailImages[0].texture = null;
        ThumbnailImages[1].texture = null;
        ThumbnailImages[2].texture = null;
        oldNameFile = new[] { "", "", "" };
        imageContents = new List<byte[]>();
        imageCaptureNumber = 0;
        BtnCheckIn.interactable = true;
        BtnCheckOut.interactable = false;
        InputFieldName.interactable = true;
        CameraCaptureImage.color = new Color(1, 1, 1, 0);
    }

    private void ResetCheckIn()
    {
        ThumbnailImages[0].texture = null;
        ThumbnailImages[1].texture = null;
        ThumbnailImages[2].texture = null;
        oldNameFile = new[] { "", "", "" };
        imageContents = new List<byte[]>();
        imageCaptureNumber = 0;
        BtnCheckIn.interactable = false;
        BtnCheckOut.interactable = true;
        InputFieldName.interactable = false;
        CameraCaptureImage.color = new Color(1, 1, 1, 0);
    }

    private void ResetCheckOut()
    {
        ThumbnailImages[0].texture = null;
        ThumbnailImages[1].texture = null;
        ThumbnailImages[2].texture = null;
        oldNameFile = new[] { "", "", "" };
        imageContents = new List<byte[]>();
        imageCaptureNumber = 0;
        BtnCheckIn.interactable = false;
        BtnCheckOut.interactable = true;
        InputFieldName.interactable = false;
        CameraCaptureImage.color = new Color(1, 1, 1, 0);
    }
}
