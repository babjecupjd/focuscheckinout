﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Text.RegularExpressions;

public class Login : MonoBehaviour
{

    #region Declare Variables

    private const string URL = "http://pepsi.focusmarketing.vn/PEPSI_TET_2016/Module/";

    public InputField InputFieldUsername, InputFieldPassword;

    public Button BtnLogin;

    #endregion Declare Variables

    void Start()
    {
        InputFieldUsername.text = PlayerPrefs.GetString("UserName", "");

        BtnLogin.onClick.AddListener(() =>
            {
                DoLogin();
            });
    }

    private void DoLogin()
    {
        if (InputFieldUsername.text.Trim() != "" && InputFieldPassword.text.Trim() != "")
        {
            BtnLogin.interactable = false;

            Dictionary<string, string> data = new Dictionary<string, string>();
            data.Add("username", InputFieldUsername.text.Trim());
            data.Add("password", Md5Sum(InputFieldPassword.text.Trim()));
            Request("Login.php", data);
        }
    }

    private void Request(string typeRequest, Dictionary<string, string> data, Dictionary<string, byte[]> images = null)
    {
        WWWForm form = new WWWForm();
        Dictionary<string, string> headers = form.headers;
        headers["Content-Type"] = "application/json";

        foreach (var pair in data)
        {
            form.AddField(pair.Key, pair.Value);
        }

        if (images != null)
        {
            foreach (var pair in images)
            {
                form.AddBinaryData("image", pair.Value, pair.Key, "image/jpg");
            }
        }

        WWW www = new WWW(URL + typeRequest, form);
        StartCoroutine(WaitForRequest(www));
    }

    IEnumerator WaitForRequest(WWW www)
    {
        yield return www;
        // check for errors
        if (www.error == null)
        {
            Debug.Log("WWW Ok!: " + www.text);
            if (www.text.Contains("\"status\":1"))
            {
                PlayerPrefs.SetString("UserName", InputFieldUsername.text.Trim());

                Regex regex = new Regex("\"user_id\":\"\\d+\"");
                GeneralVariables.USERID = regex.Match(www.text).ToString().Replace("\"", "").Replace("user_id:", "");
                GeneralVariables.USERNAME = InputFieldUsername.text.Trim();
                Application.LoadLevel("Check");
            }
            else
            {
                BtnLogin.interactable = true;
            }
        }
        else
        {
            Debug.Log("WWW Error: " + www.error);
            BtnLogin.interactable = true;
        }
    }

    public string Md5Sum(string strToEncrypt)
    {
        System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
        byte[] bytes = ue.GetBytes(strToEncrypt);

        System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] hashBytes = md5.ComputeHash(bytes);

        string hashString = "";

        for (int i = 0; i < hashBytes.Length; i++)
        {
            hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }

        return hashString.PadLeft(32, '0');
    }
}
