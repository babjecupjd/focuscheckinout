﻿using UnityEngine;
using System.Collections;

public class UIKeepAspectRatio : MonoBehaviour
{

    #region Declare Variables

    public float Width = 1024;
    public float Height = 600;

    #endregion Declare Variables

    void Awake () {
        GetComponent<Camera>().aspect = Width / Height;
	}
	
}
